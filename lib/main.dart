import 'package:direct_to_dungeon/view/Searching_Page.dart';
import 'package:direct_to_dungeon/view/admin/Admin_Panel.dart';
import 'package:direct_to_dungeon/view/admin/reports/Report_Page.dart';
import 'package:direct_to_dungeon/view/admin/reports/User_Reports.dart';
import 'package:direct_to_dungeon/view/conversation/Conversation.dart';
import 'package:direct_to_dungeon/view/conversation/Conversation_List_Page.dart';
import 'package:direct_to_dungeon/view/information/Information_Page.dart';
import 'package:direct_to_dungeon/view/settings/Settings_Page.dart';
import 'package:direct_to_dungeon/view/user/Edit_Profile_Page.dart';
import 'package:direct_to_dungeon/view/user/Upload_Page.dart';
import 'package:direct_to_dungeon/view/user/User_Profile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(GetMaterialApp(
    initialRoute: '/Search_Page',
    getPages: [
      GetPage(name: '/Search_Page', page: () => SearchPage()),
      GetPage(name: '/User_Profile', page: () => ProfilePage()),
      GetPage(name: '/Edit_Profile', page: () => EditProfile()),
      GetPage(name: '/Upload_Page', page:  () => UploadPage()),
      GetPage(name: '/Admin_Page', page:  () => AdminPage()),
      GetPage(name: '/Report_Page', page:  () => ReportPage()),
      GetPage(name: '/User_Reports', page:  () => UserReportPage()),
      GetPage(name: '/Conversation_List', page:  () => ConversationPage()),
      GetPage(name: '/ViewConversation', page:  () => ViewConversationPage()),
      GetPage(name: '/Settings_Page', page:  () => SettingsPage()),
      GetPage(name: '/Information_Page', page:  () => InformationPage()),
    ],
  ));
}
