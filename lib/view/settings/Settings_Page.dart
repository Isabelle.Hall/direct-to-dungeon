import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  // --InitState--
  @override
  void initState() {
    super.initState();
  }

  //--Functions--


  // --Main bulk--
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: <Widget>[
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          color: Colors.red,
                          height: MediaQuery.of(context).size.height - 80,
                          width: MediaQuery.of(context).size.width - 80,
                          child: Column(
                            children: [
                              Row(
                                children: [Text("data")],
                              ),
                              Spacer(),
                              Row(
                                children: [Text("data")],
                              ),
                              Spacer(),
                              Row(
                                children: [Text("data")],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}