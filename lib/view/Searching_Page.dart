import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  // --Variables--

  // --InitState--
  @override
  void initState() {
    _getAccount();
    super.initState();
  }

  //--Functions--
  Future<void> _update() async {
    _getAccount();
    _getNotices();
  }

  Future<void> _getAccount() async {}

  Future<void> _getNotices() async {}

  findOthers() {}

  matches() {}

  settings() {}

  @override
  void dispose() {
    super.dispose();
  }

  profileCard() {
    return Card(
      elevation: 10,
      child: Container(
        width: MediaQuery.of(context).size.width - 50,
        height: MediaQuery.of(context).size.height - 200,
        child: Column(
          children: [
            ListTile(
              leading: CircleAvatar(),
              title: Text("name"),
            ),
            Divider(
              thickness: 3,
            ),
            Expanded(
              child: Container(
                color: Colors.red,
              ),
            ),
            Divider(
              thickness: 3,
            ),
            ListTile(
                leading: Icon(Icons.add),
                title: Center(
                    child: SmoothStarRating(
                  rating: 2,
                  starCount: 5,
                  allowHalfRating: true,
                )),
                trailing: Icon(Icons.remove)),
          ],
        ),
      ),
    );
  }

  // --Main bulk--
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LayoutBuilder(
        builder: (context, constraints) => ListView(
          scrollDirection: Axis.horizontal,
          children: [
            Container(
                padding: const EdgeInsets.all(20.0),
                constraints: BoxConstraints(
                  minHeight: constraints.maxHeight,
                ),
                child: Center(child: profileCard()))
          ],
        ),
      ),
    );
  }
}
