import 'dart:ffi';
import 'dart:js';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:video_player/video_player.dart';

import 'Conversation.dart';

class User {
  int id;
  double rating;
  List<int> conversations;
  List<String> systemsPlayed;
  List<String> pictures;
  String description;
  String name;
  String videoAddress;
  String avatar;
  Map type = {"GM":0, "Player": 0};
  List<Conversation> commonConversations = [];
  String lastActive;
  String status;

  User(this.id, this.conversations, this.systemsPlayed, this.pictures, this.description, this.name, this.videoAddress, this.avatar, this.rating, this.type, this.commonConversations, this.lastActive,
      this.status);


  Widget render({Function onTap, Widget trailing})
  {
    return Column(
      children: [
        ListTile(
          leading: CircleAvatar(backgroundImage: this.avatar != null ? NetworkImage(this.avatar) : null),
          title: Text(this.name),
        ),
        Divider(
          thickness: 3,
        ),
        Expanded(
          child: Container(
            color: Colors.red,
          ),
        ),
        Divider(
          thickness: 3,
        ),
        ListTile(
            leading: Icon(Icons.add),
            title: Center(
                child: SmoothStarRating(
                  rating: this.rating,
                  starCount: 5,
                  allowHalfRating: true,
                )),
            trailing: Icon(Icons.remove)),
      ],
    );
  }
}