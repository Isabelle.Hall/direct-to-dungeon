import 'package:flutter/cupertino.dart';
import 'User.dart';

class Report {
  int id;
  String title;
  String createdFormatted;
  String category;
  User user;
  Host host;

  Report(this.id, this.title, this.createdFormatted, this.category, this.user, this.host);
}

class Host {
  String type;
  int id;
  String message;

  Host({this.type, this.id, this.message});

  factory Host.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return Host(type: json["type"], id: json["id"], message: json["name"]);
  }
}
