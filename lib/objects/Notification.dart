import 'package:flutter/cupertino.dart';

class Notification {
  dynamic createdAt;
  String message;

  Notification(this.createdAt, this.message);
}