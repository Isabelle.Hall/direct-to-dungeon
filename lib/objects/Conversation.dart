import 'package:flutter/cupertino.dart';

import 'User.dart';

class Conversation {
  int id;
  int members;
  String title;
  String userList;
  String avatar;
  String status;
  String last;
  String updated;
  DateTime updatedUnix;
  List<User> users;


  Conversation(this.id, this.members, this.title, this.userList, this.avatar, this.status, this.last, this.updated, this.updatedUnix, this.users);
}